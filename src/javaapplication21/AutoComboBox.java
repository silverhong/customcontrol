/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication21;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class AutoComboBox extends JComboBox<Object> {
    String Items[] = {"Livid", "Testing", "SO9"};
    public AutoComboBox() {
        setModel(new DefaultComboBoxModel(Items));
        setSelectedIndex(-1);
        AutoCompleteDecorator.decorate(this);
    }
    public void setItems(String[] Items) {
        this.Items = Items;
    }

    @Override
    public void setModel(ComboBoxModel<Object> aModel) {
        super.setModel(aModel); //To change body of generated methods, choose Tools | Templates.
        setSelectedIndex(-1);
    }
    
}
